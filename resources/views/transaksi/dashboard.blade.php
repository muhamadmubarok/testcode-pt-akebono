@extends('layouts.cms')
@section('title', 'Dashboard')

@section('files-css')
<style type="text/css">/* Chart.js */
    @keyframes chartjs-render-animation{from{opacity:.99}to{opacity:1}}.chartjs-render-monitor{animation:chartjs-render-animation 1ms}.chartjs-size-monitor,.chartjs-size-monitor-expand,.chartjs-size-monitor-shrink{position:absolute;direction:ltr;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1}.chartjs-size-monitor-expand>div{position:absolute;width:1000000px;height:1000000px;left:0;top:0}.chartjs-size-monitor-shrink>div{position:absolute;width:200%;height:200%;left:0;top:0}</style>
@endsection

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    {{-- <li class="breadcrumb-item active">Blank Page</li> --}}
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif
    <!-- Default box -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-6">
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ $list_item->count() }}</h3>
                        <p>Produk</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{ $list_transaksi->qty_actual }}</h3>
                        <p>Quantity Produk</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3>{{ $list_lokasi->count() }}</h3>
                        <p>Lokasi</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-6">
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3>{{ $list_karyawan->count() }}</h3>
                        <p>Karyawan</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-body">
                        <form method="GET" action="{{ route('dashboard') }}">
                            <div class="row">
                                <div class="col-md-3">Tahun</div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <select class="form-control" name="tahun">
                                            <option value="">Pilih Tahun</option>
                                            @for ($tahun=date('Y'); $tahun > 2014 ; $tahun--)
                                            <option value="{{$tahun}}" @if($tahun == $tahun_selected) selected @endif>{{$tahun}}</option>

                                            @endfor
                                        </select>
                                        <span class="input-group-append">
                                            <button type="submit" class="form-control"><i class="fas fa-search"></i> &nbsp; Cari</button>
                                        </span>
                                    </div>


                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Grafik Item</h3>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand"><div class=""></div></div>
                                <div class="chartjs-size-monitor-shrink"><div class=""></div></div>
                            </div>
                            <canvas id="areaChartItem" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Grafik Lokasi</h3>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand"><div class=""></div></div>
                                <div class="chartjs-size-monitor-shrink"><div class=""></div></div>
                            </div>
                            <canvas id="lineChartLokasi" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Grafik Karyawan</h3>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand"><div class=""></div></div>
                                <div class="chartjs-size-monitor-shrink"><div class=""></div></div>
                            </div>
                            <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Grafik Total Item</h3>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand"><div class=""></div></div>
                                <div class="chartjs-size-monitor-shrink"><div class=""></div></div>
                            </div>
                            <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection
@section('files-js')
<script src="adminlte/plugins/chart.js/Chart.min.js"></script>
<script>
    function random_rgba() {
        var o = Math.round, r = Math.random, s = 255;
        return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + '0.8'+ ')';
    }

    $(function() {
    /* Chart
    * -------
    */
    labels          = ['January', 'February', 'March', 'April', 'May', 'June','July','August','September', 'October', 'November', 'December'];

    //--------------
    //- AREA CHART -
    //--------------
    list_item       = @json($list_item)

    list_transaksi  = @json($list_transaksi_by_item)

    var new_dataset = []

    list_item.forEach(item => {

        var color = random_rgba();

        dataset_object  = list_transaksi.filter(function (el) {
            return item.kode_item == el.kode;
        });

        dataset_value = []

        labels.forEach(month => {
            dataset_quantity    = dataset_object.filter(function (el){
                return month == el.month_name;
            })

            dataset_sum     = (dataset_quantity.length > 0) ? dataset_quantity[0].quantity : 0;
            dataset_value.push(dataset_sum)
        });

        item_dataset    = {
            label: item.nama_item,
            backgroundColor: color,
            borderColor: color,
            pointRadius: false,
            pointColor: '#3b8bba',
            pointStrokeColor: color,
            pointHighlightFill: '#fff',
            pointHighlightStroke: color,
            data: dataset_value
        }

        new_dataset.push(item_dataset);
    });

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChartItem').get(0).getContext('2d')

    var areaChartData = {
        labels: labels,
        datasets: new_dataset
    }

    var areaChartOptions = {
        maintainAspectRatio: false,
        responsive: true,
        legend: {
            position: 'bottom',
            display: true
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: true,
                }
            }],
            yAxes: [{
                gridLines: {
                    display: true,
                }
            }]
        }
    }

    // This will get the first returned node in the jQuery collection.
    new Chart(areaChartCanvas, {
        type: 'line',
        data: areaChartData,
        options: areaChartOptions
    })

    //-------------
    //- LINE CHART -
    //--------------
    list_lokasi     = @json($list_lokasi)

    list_transaksi_lokasi  = @json($list_transaksi_by_lokasi)

    var new_dataset_lokasi = []

    list_lokasi.forEach(lokasi => {

        var color = random_rgba();

        dataset_object  = list_transaksi_lokasi.filter(function (el) {
            return lokasi.kode_lokasi == el.lokasi;
        });

        new_dataset_lokasi_sum = []

        labels.forEach(month => {
            dataset_quantity    = dataset_object.filter(function (el){
                return month == el.month_name;
            })

            dataset_sum     = (dataset_quantity.length > 0) ? dataset_quantity[0].quantity : 0;
            new_dataset_lokasi_sum.push(dataset_sum)
        });

        lokasi_dataset    = {
            label: lokasi.nama_lokasi,
            backgroundColor: color,
            borderColor: color,
            pointRadius: false,
            pointColor: '#3b8bba',
            pointStrokeColor: color,
            pointHighlightFill: '#fff',
            pointHighlightStroke: color,
            data: new_dataset_lokasi_sum,
            fill:false
        }

        new_dataset_lokasi.push(lokasi_dataset);
    });

    var lineChartData = {
        labels: labels,
        datasets: new_dataset_lokasi
    }

    var lineChartCanvas = $('#lineChartLokasi').get(0).getContext('2d')
    var lineChartOptions = $.extend(true, {}, areaChartOptions)

    lineChartOptions.datasetFill = false

    var lineChart = new Chart(lineChartCanvas, {
        type: 'line',
        data: lineChartData,
        options: lineChartOptions
    })


    //-------------
    //- BAR CHART -
    //-------------
    list_karyawan               = @json($list_karyawan)

    list_transaksi_karyawan     = @json($list_transaksi_by_karyawan)

    var new_dataset_karyawan    = []

    list_karyawan.forEach(karyawan => {

        var color = random_rgba();

        dataset_object  = list_transaksi_karyawan.filter(function (el) {
            return karyawan.npk == el.npk;
        });

        new_dataset_karyawan_sum = []

        labels.forEach(month => {
            dataset_quantity    = dataset_object.filter(function (el){
                return month == el.month_name;
            })

            dataset_sum     = (dataset_quantity.length > 0) ? dataset_quantity[0].quantity : 0;
            new_dataset_karyawan_sum.push(dataset_sum)
        });

        karyawan_dataset    = {
            label: karyawan.nama,
            backgroundColor: color,
            borderColor: color,
            pointRadius: false,
            pointColor: '#3b8bba',
            pointStrokeColor: color,
            pointHighlightFill: '#fff',
            pointHighlightStroke: color,
            data: new_dataset_karyawan_sum,
            fill:false
        }

        new_dataset_karyawan.push(karyawan_dataset);
    });

    var barChartData = {
        labels: labels,
        datasets: new_dataset_karyawan
    }

    var barChartCanvas = $('#barChart').get(0).getContext('2d')

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }

    new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    })

    //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    // var donutChartCanvas = $('#donutChart').get(0).getContext('2d')

    list_transaksi_total_item   = @json($list_transaksi_total_item)


    pie_label       = [];
    pie_dataset     = [];
    pie_color       = [];

    list_item.forEach(item => {
        pie_label.push(item.nama_item)

        pie_value   = list_transaksi_total_item.filter(function (el, i){
            return item.kode_item == el.kode;
        }, item)

        pie_dataset.push(pie_value[0].qty_actual)
        pie_color.push(random_rgba())

    });

    console.log(pie_label);

    var donutData = {
        labels: pie_label,
        datasets: [{
            data: pie_dataset,
            backgroundColor: pie_color,
        }]
    }
    var donutOptions = {
        maintainAspectRatio: false,
        responsive: true,
    }
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData = donutData;
    var pieOptions = {
        maintainAspectRatio: false,
        responsive: true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
        type: 'pie',
        data: pieData,
        options: pieOptions
    })

})
</script>
@endsection

