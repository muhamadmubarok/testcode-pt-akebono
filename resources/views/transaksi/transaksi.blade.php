@extends('layouts.cms')
@section('title', 'Transaksi')
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Transaksi View</h1>
            </div>
        </div>
    </div>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Transaksi</h3>
        </div>
        <div class="card-body">
            <form method="GET" action="{{ route('transaksi') }}">
                <div class="row">
                    <div class="col-md-3">Tanggal Transaksi</div>
                    <div class="col-md-3">

                        <input type="date" name="tanggal_transaksi" class="form-control" placeholder="Tanggal Transaksi" />
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-3">Lokasi</div>
                    <div class="col-md-3">
                        <select class="form-control" name="lokasi">
                            <option value="">Pilih Lokasi</option>

                            @foreach ($list_lokasi as $key => $lokasi)
                                <option value="{{ $lokasi->kode_lokasi }}">{{ $lokasi->nama_lokasi }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-5">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-sm btn-success"> <i class="fas fa-search"></i> &nbsp; Cari</button>
                            <a href="{{ route('transaksi') }}" class="btn btn-sm btn-danger"> <i class="fa fa-times"></i>  &nbsp; Clear</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
                <thead class="bg-success">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Kode Item</th>
                        <th>Nama Item</th>
                        <th>Kode Lokasi</th>
                        <th>Nama Lokasi</th>
                        <th>Qty Actual</th>
                        <th>Created By</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list_transaksi as $key => $transaksi)
                    <tr>
                        <td> {{ ($list_transaksi->currentpage()-1) * $list_transaksi->perpage() + $key + 1 }} </td>
                        <td> {{ \Carbon\Carbon::parse($transaksi->tanggal_transaksi)->format('d F Y') }} </td>
                        <td> {{ $transaksi->kode }} </td>
                        <td> {{ $transaksi->nama_item }} </td>
                        <td> {{ $transaksi->lokasi }} </td>
                        <td> {{ $transaksi->nama_lokasi }} </td>
                        <td> {{ $transaksi->qty_actual }} </td>
                        <td> {{ $transaksi->nama }} </td>
                        <td> <a href="{{ route('transaksi.show', ['id'=>$transaksi->id]) }}" class="btn btn-xs btn-info"> <i class="fa fa-pencil-alt"></i>  &nbsp; Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            {!! $list_transaksi->links()  !!}
        </div>
        <!-- /.card-footer-->
    </div>
    <!-- /.card -->
</section>
@endsection
