@extends('layouts.cms')
@section('title', 'Produksi')
@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tambah Transaksi</h1>
            </div>
        </div>
    </div>
</section>
<section class="content">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Transaksi</h3>
        </div>
        <div class="card-header">
            <form method="POST" action="{{ route('transaksi.store') }}">
                @csrf
                <div class="row">
                    <div class="col-md-3">Tanggal Transaksi</div>
                    <div class="col-md-3">

                        <input type="date" name="tanggal_transaksi" class="form-control  @error('tanggal_transaksi') is-invalid @enderror" placeholder="Tanggal Transaksi" value="{{date("Y-m-d")}}" />
                        @if ($errors->has('tanggal_transaksi'))
                            <span class="text-danger">{{ $errors->first('tanggal_transaksi') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-3">Lokasi</div>
                    <div class="col-md-3">
                        <select class="form-control  @error('lokasi') is-invalid @enderror" name="lokasi">
                            <option value="">Pilih Lokasi</option>

                            @foreach ($list_lokasi as $key => $lokasi)
                                <option value="{{ $lokasi->kode_lokasi }}">{{ $lokasi->nama_lokasi }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('lokasi'))
                            <span class="text-danger">{{ $errors->first('lokasi') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-3">Item</div>
                    <div class="col-md-3">
                        <select class="form-control  @error('kode') is-invalid @enderror" name="kode">
                            <option value="">Pilih Item</option>

                            @foreach ($list_item as $key => $item)
                                <option value="{{ $item->kode_item }}">{{ $item->nama_item }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('kode'))
                            <span class="text-danger">{{ $errors->first('kode') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row  mt-2">
                    <div class="col-md-3">Quantity</div>
                    <div class="col-md-3">

                        <input type="number" name="qty_actual" class="form-control  @error('qty_actual') is-invalid @enderror" placeholder="Quantity" min="0" oninput="this.value = !!this.value && Math.abs(this.value) >= 0 ? Math.abs(this.value) : null"/>
                        @if ($errors->has('qty_actual'))
                            <span class="text-danger">{{ $errors->first('qty_actual') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-5">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-sm btn-success"> <i class="fas fa-save"></i> &nbsp; Simpan</button>
                            <a href="{{ route('transaksi.create') }}" class="btn btn-sm btn-danger"> <i class="fa fa-times"></i>  &nbsp; Clear</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
