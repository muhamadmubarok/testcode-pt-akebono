<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use App\Models\Item;
use App\Models\Karyawan;
use App\Models\Lokasi;
use App\Models\Produksi;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\Datatables;

class TransaksiController extends Controller
{
    public function dashboard(Request $request)
    {
        $tahun              = ($request->get('tahun') == null) ? date('Y') : $request->get('tahun');
        $list_item          = Item::select('kode as kode_item','nama_item')->get();
        $list_lokasi        = Lokasi::select('kode as kode_lokasi','nama_lokasi')->get();
        $list_karyawan      = Karyawan::select('npk','nama')->get();
        $list_transaksi     = Produksi::select(DB::raw('sum(qty_actual) as qty_actual'))->first();

        $list_transaksi_by_item = DB::select("SELECT
                mi.nama_item nama_item,
                tp.kode kode,
                COUNT(tp.id) count,
                sum(tp.qty_actual) quantity,
                year(tp.tanggal_transaksi) years,
                MONTH(tp.tanggal_transaksi) months,
                MONTHNAME(tp.tanggal_transaksi) month_name
            FROM
                `transaksi_produksi` as tp
                JOIN master_item as mi ON mi.kode = tp.kode
            WHERE
                year(tp.tanggal_transaksi) = '$tahun'
            GROUP BY
                years, month_name, months, kode ,nama_item
            ORDER BY
                nama_item ASC, months ASC");

        $list_transaksi_by_lokasi = DB::select("SELECT
            ml.nama_lokasi nama_lokasi,
            tp.lokasi lokasi,
            COUNT(tp.id) count,
            sum(tp.qty_actual) quantity,
            year(tp.tanggal_transaksi) years,
            MONTH(tp.tanggal_transaksi) months,
            MONTHNAME(tp.tanggal_transaksi) month_name
        FROM
            `transaksi_produksi` as tp
            JOIN master_lokasi as ml ON ml.kode = tp.lokasi
        WHERE
            year(tp.tanggal_transaksi) = '$tahun'
        GROUP BY
            years, month_name, months, lokasi ,nama_lokasi
        ORDER BY
            nama_lokasi ASC, months ASC");

        $list_transaksi_by_karyawan = DB::select("SELECT
            mk.nama nama,
            tp.npk npk,
            COUNT(tp.id) count,
            sum(tp.qty_actual) quantity,
            year(tp.tanggal_transaksi) years,
            MONTH(tp.tanggal_transaksi) months,
            MONTHNAME(tp.tanggal_transaksi) month_name
        FROM
            `transaksi_produksi` as tp
            JOIN master_karyawan as mk ON mk.npk = tp.npk
        WHERE
            year(tp.tanggal_transaksi) = '$tahun'
        GROUP BY
            years, month_name, months, npk, nama
        ORDER BY
            nama ASC, months ASC");

        $list_transaksi_total_item     = Produksi::select(DB::raw('sum(qty_actual) as qty_actual'), 'kode')
            ->whereYear('tanggal_transaksi', $tahun)
            ->groupBy('kode')
            ->get();

        $data = array(
            'user' => $request->session()->get('login'),
            'tahun_selected' => $tahun,

            'list_transaksi' => $list_transaksi,
            'list_transaksi_by_item' => $list_transaksi_by_item,
            'list_transaksi_by_lokasi' => $list_transaksi_by_lokasi,
            'list_transaksi_by_karyawan' => $list_transaksi_by_karyawan,
            'list_transaksi_total_item' => $list_transaksi_total_item,

            'list_item' => $list_item,
            'list_lokasi' => $list_lokasi,
            'list_karyawan' => $list_karyawan
        );

        return view('transaksi.dashboard', $data);
    }

    public function transaksi(Request $request)
    {
        $search             = $request->all();
        $list_lokasi        = Lokasi::select('kode as kode_lokasi','nama_lokasi')->get();
        $list_transaksi     = Produksi::join('master_karyawan', 'transaksi_produksi.npk', '=', 'master_karyawan.npk')
            ->join('master_lokasi', 'transaksi_produksi.lokasi', '=', 'master_lokasi.kode')
            ->join('master_item', 'transaksi_produksi.kode', '=', 'master_item.kode');

        if ($request->get('tanggal_transaksi') != null) {
            $list_transaksi->whereDate('tanggal_transaksi', '=', $request->get('tanggal_transaksi'));
        }

        if ($request->get('lokasi') != null) {
            $list_transaksi->where('transaksi_produksi.lokasi', '=', $request->get('lokasi'));
        }

        $list_transaksi->orderBy('tanggal_transaksi', 'DESC');

        $data = array(
            'user'              => $request->session()->get('login'),
            'search'            => $search,
            'list_transaksi'    => $list_transaksi->paginate(),
            'list_lokasi'       => $list_lokasi
        );

        return view('transaksi.transaksi', $data);
    }

    public function create(Request $request)
    {
        $list_item          = Item::select('kode as kode_item','nama_item')->get();
        $list_lokasi        = Lokasi::select('kode as kode_lokasi','nama_lokasi')->get();

        $data = array(
            'user'              => $request->session()->get('login'),
            'list_lokasi'       => $list_lokasi,
            'list_item'         => $list_item
        );

        return view('transaksi.create', $data);
    }

    public function store(Request $request)
    {
        $credentials = $request->validate([
            'tanggal_transaksi' => 'required',
            'lokasi'            => 'required',
            'kode'              => 'required',
            'qty_actual'        => 'required|numeric|min:0|not_in:0'
        ]);

        $transaksi   = array_merge($request->all(),array('npk'=>$request->session()->get('login')->npk));
        Produksi::create($transaksi);

        return back()->with(['success' => 'Berhasil menyimpan data']);
    }

    public function show(Request $request, $id)
    {
        $transaksi          = Produksi::find($id);
        $list_item          = Item::select('kode as kode_item','nama_item')->get();
        $list_lokasi        = Lokasi::select('kode as kode_lokasi','nama_lokasi')->get();

        $data = array(
            'user'              => $request->session()->get('login'),
            'list_lokasi'       => $list_lokasi,
            'list_item'         => $list_item,
            'transaksi'         => $transaksi
        );

        return view('transaksi.show', $data);
        dd($transaksi);
    }

    public function update(Request $request, $id)
    {
        $credentials = $request->validate([
            'tanggal_transaksi' => 'required',
            'lokasi'            => 'required',
            'kode'              => 'required',
            'qty_actual'        => 'required|numeric|min:0|not_in:0'
        ]);

        $input      = $request->all();
        $transaksi  = Produksi::find($id);

        $transaksi->tanggal_transaksi   = $input['tanggal_transaksi'];
        $transaksi->lokasi              = $input['lokasi'];
        $transaksi->kode                = $input['kode'];
        $transaksi->qty_actual          = $input['qty_actual'];

        $transaksi->save();

        return redirect('/transaksi')->with(['success' => 'Berhasil menyimpan data']);
    }
}
