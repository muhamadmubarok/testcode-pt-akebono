<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Login;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if($request->session()->has('login'))
        {
            return redirect('/dashboard');
        }
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $username   = $request->get('username');
        $password   = $request->get('password');

        $user       = Login::join('master_karyawan', 'login.username', '=', 'master_karyawan.npk')
            ->where('username', $username)
            ->where('password', md5($password))
            ->first();

        if(isset($user))
        {
            $request->session()->put('login', $user);
            return redirect('/dashboard')->with(['success' => 'Berhasil melakukan login']);
        }

        return back()->withErrors([
            'username' => 'Data yang ada masukan tidak sesuai.',
        ])->onlyInput('username');

    }

    public function logout(Request $request)
    {
        if($request->session()->has('login'))
        {
            $request->session()->forget('login');
        }
        return redirect('/dashboard');
    }

    public function dashboard(Request $request)
    {
        # code...
    }
}
