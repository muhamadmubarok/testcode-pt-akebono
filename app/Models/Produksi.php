<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Produksi extends Model
{
    use HasFactory;

    protected $table        = 'transaksi_produksi';
    protected $primaryKey   = 'id';
    public $timestamps      = false;
    protected $softDelete   = false;
    protected $fillable = [
        'npk',
        'tanggal_transaksi',
        'lokasi',
        'kode',
        'qty_actual'
    ];

    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])
            ->format('d, M Y H:i');
    }
}


