<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Planning extends Model
{
    use HasFactory;
    protected $table        = 'master_planning';
    protected $primaryKey   = 'kode';
    public $timestamps      = false;
    protected $softDelete   = false;
    protected $fillable = [
        'kode',
        'qty_target',
        'waktu_target'
    ];
}
