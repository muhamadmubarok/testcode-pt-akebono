<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Login extends Model
{
    use HasFactory;

    protected $table        = 'login';
    protected $primaryKey   = 'username';
    public $timestamps      = false;
    protected $softDelete   = false;
    protected $fillable = [
        'username',
        'password',
        'created_date',
        'created_by'
    ];
    protected $hidden = [
        'password',
    ];
}
