<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    use HasFactory;

    protected $table        = 'master_lokasi';
    protected $primaryKey   = 'kode';
    public $timestamps      = false;
    protected $fillable = [
        'kode',
        'nama_lokasi'
    ];
}
