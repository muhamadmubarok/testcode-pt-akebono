<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $table        = 'master_item';
    protected $primaryKey   = 'kode';
    public $timestamps      = false;
    protected $softDelete   = false;
    protected $fillable = [
        'kode',
        'nama_item'
    ];
}
