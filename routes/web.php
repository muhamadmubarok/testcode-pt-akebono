<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return redirect()->route('dashboard');
});

Route::controller(App\Http\Controllers\Auth\AuthController::class)->group(function() {
    Route::get('/login',            'login')->name('login');
    Route::post('/authenticate',    'authenticate')->name('authenticate');
    Route::get('/logout',           'logout')->name('logout');
});


Route::controller(App\Http\Controllers\TransaksiController::class)->group(function() {
    Route::get('/dashboard',            'dashboard')->name('dashboard')->middleware('check.login');
    Route::get('/transaksi',            'transaksi')->name('transaksi')->middleware('check.login');
    Route::get('/show/{id}',            'show')->name('transaksi.show')->middleware('check.login');
    Route::get('/input',                'create')->name('transaksi.create')->middleware('check.login');
    Route::post('/store',               'store')->name('transaksi.store')->middleware('check.login');
    Route::put('/update/{id}',          'update')->name('transaksi.update')->middleware('check.login');
});

