<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('master_achivement', function (Blueprint $table) {
            $table->string('kode',              4);
            $table->time('time_from',           $precision = 0);
            $table->time('time_to',             $precision = 0);

            $table->primary('kode');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('master_achivement');
    }
};
