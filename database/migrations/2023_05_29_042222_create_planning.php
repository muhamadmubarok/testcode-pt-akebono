<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('master_planning', function (Blueprint $table) {
            $table->string('kode',              4);
            $table->integer('qty_target');
            $table->double('waktu_target'       , 8, 0);

            $table->foreign('kode')->references('kode')->on('master_item');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('master_planning');
    }
};
