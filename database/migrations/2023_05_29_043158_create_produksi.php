<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transaksi_produksi', function (Blueprint $table) {
            $table->id();
            $table->string('npk',                   5);
            $table->timestamp('tanggal_transaksi',  $precision = 0);
            $table->string('lokasi',                5);
            $table->string('kode',                  5);
            $table->integer('qty_actual');
            
            $table->foreign('npk')->references('npk')->on('master_karyawan');
            $table->foreign('lokasi')->references('kode')->on('master_lokasi');
            $table->foreign('kode')->references('kode')->on('master_item');
        });

        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transaksi_produksi');
    }
};
