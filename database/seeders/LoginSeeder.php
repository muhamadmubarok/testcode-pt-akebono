<?php

namespace Database\Seeders;

use App\Models\Login;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LoginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $list_login          = array();
        $list_login[]        = array('username'=>'10001','password'=>md5('password'),'created_date'=>date('Y-m-d H:i:s', strtotime('01-01-2021 01:08:00')),'created_by'=>'System');
        $list_login[]        = array('username'=>'10002','password'=>md5('password'),'created_date'=>date('Y-m-d H:i:s', strtotime('01-01-2021 01:08:00')),'created_by'=>'System');
        foreach ($list_login as $key => $login) {
            Login::create($login);
        }
    }
}
