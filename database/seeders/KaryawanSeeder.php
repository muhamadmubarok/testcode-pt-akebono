<?php

namespace Database\Seeders;

use App\Models\Karyawan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $list_karyawan          = array();
        $list_karyawan[]        = array('npk'=>'10001','nama'=>'Agus','alamat'=>'Jakarta');
        $list_karyawan[]        = array('npk'=>'10002','nama'=>'Asep','alamat'=>'Purbalingga');
        $list_karyawan[]        = array('npk'=>'10003','nama'=>'Jajang','alamat'=>'Subang');
        $list_karyawan[]        = array('npk'=>'10004','nama'=>'Ahmad','alamat'=>'Bandung');

        foreach ($list_karyawan as $key => $karyawan) {
            Karyawan::create($karyawan);
        }
    }
}
