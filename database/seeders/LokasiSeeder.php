<?php

namespace Database\Seeders;

use App\Models\Lokasi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LokasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {        
        $list_lokasi        = array();
        $list_lokasi[]      = array('kode'=>'L001','nama_lokasi'=>'Lokasi 1');
        $list_lokasi[]      = array('kode'=>'L002','nama_lokasi'=>'Lokasi 2');
        $list_lokasi[]      = array('kode'=>'L003','nama_lokasi'=>'Lokasi 3');
        $list_lokasi[]      = array('kode'=>'L004','nama_lokasi'=>'Lokasi 4');

        foreach ($list_lokasi as $key => $lokasi) {
            Lokasi::create($lokasi);
        }
    }
}
