<?php

namespace Database\Seeders;

use App\Models\Achivement;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AchivementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $prefix     = 'A';
        $hour_from  = 7;

        for ($i=1; $i < 14; $i++) {
            $number     = str_pad($i, 3, '0', STR_PAD_LEFT);
            $kode       = $prefix.$number;

            $hour_to    = $hour_from + 1;

            $time_from  = str_pad($hour_from, 2, '0', STR_PAD_LEFT).":31:00";
            $time_to    = str_pad($hour_to, 2, '0', STR_PAD_LEFT).":30:00";

            $hour_from++;
            $hour_to++;

            Achivement::create([
                'kode'          => $kode,
                'time_from'     => $time_from,
                'time_to'       => $time_to
            ]);
        }
    }
}
