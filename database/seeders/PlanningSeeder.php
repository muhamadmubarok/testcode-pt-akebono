<?php

namespace Database\Seeders;

use App\Models\Planning;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlanningSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $list_planning          = array();
        $list_planning[]        = array('kode'=>'M001','qty_target'=>10,'waktu_target'=>20);
        $list_planning[]        = array('kode'=>'M002','qty_target'=>15,'waktu_target'=>30);
        $list_planning[]        = array('kode'=>'M003','qty_target'=>12,'waktu_target'=>24);
        $list_planning[]        = array('kode'=>'M004','qty_target'=>14,'waktu_target'=>28);

        foreach ($list_planning as $key => $planning) {
            Planning::create($planning);
        }
    }
}
