<?php

namespace Database\Seeders;

use App\Models\Produksi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProduksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $list_produksi          = array();
        $list_produksi[]        = array('npk'=>'10001','tanggal_transaksi'=>date('Y-m-d H:i:s', strtotime('05-01-2021 09:31:01')),'lokasi'=>'L001','kode'=>'M001','qty_actual'=>'10');
        $list_produksi[]        = array('npk'=>'10002','tanggal_transaksi'=>date('Y-m-d H:i:s', strtotime('12-01-2021 09:32:01')),'lokasi'=>'L002','kode'=>'M002','qty_actual'=>'20');
        $list_produksi[]        = array('npk'=>'10003','tanggal_transaksi'=>date('Y-m-d H:i:s', strtotime('12-01-2021 09:33:01')),'lokasi'=>'L001','kode'=>'M001','qty_actual'=>'25');
        $list_produksi[]        = array('npk'=>'10001','tanggal_transaksi'=>date('Y-m-d H:i:s', strtotime('13-01-2021 09:34:01')),'lokasi'=>'L003','kode'=>'M003','qty_actual'=>'14');
        $list_produksi[]        = array('npk'=>'10004','tanggal_transaksi'=>date('Y-m-d H:i:s', strtotime('13-01-2021 09:35:01')),'lokasi'=>'L004','kode'=>'M004','qty_actual'=>'26');
        $list_produksi[]        = array('npk'=>'10003','tanggal_transaksi'=>date('Y-m-d H:i:s', strtotime('13-01-2021 09:36:01')),'lokasi'=>'L002','kode'=>'M002','qty_actual'=>'20');

        foreach ($list_produksi as $key => $produksi) {
            Produksi::create($produksi);
        }
    }
}


