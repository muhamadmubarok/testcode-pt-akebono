<?php

namespace Database\Seeders;

use App\Models\Item;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $list_item          = array();
        $list_item[]        = array('kode'=>'M001','nama_item'=>'Bolpen');
        $list_item[]        = array('kode'=>'M002','nama_item'=>'Pensil');
        $list_item[]        = array('kode'=>'M003','nama_item'=>'Penghapus');
        $list_item[]        = array('kode'=>'M004','nama_item'=>'Spidol');

        foreach ($list_item as $key => $item) {
            Item::create($item);
        }
    }
}
